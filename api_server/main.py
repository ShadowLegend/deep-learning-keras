import cgi
import json
import io
import os

import falcon
import numpy
import PIL

from server import CatsVsDogsCNN, Cifa10CNN

class HandleCORS(object):
    def process_request(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.set_header('Access-Control-Allow-Methods', '*')
        resp.set_header('Access-Control-Allow-Headers', '*')
        resp.set_header('Access-Control-Max-Age', 1728000)

cats_vs_dogs_classifier = CatsVsDogsCNN(
    labels='./labels/cats_vs_dogs.json',
    cnn_config='./configs/cats_vs_dogs_cnn.json',
    pretrained_weight='./pretrained_weights/cats_vs_dogs.hdf5')

cifa10_classifier = Cifa10CNN(
    labels='./labels/cifa10.json',
    cnn_config='./configs/cifa10_cnn.json',
    pretrained_weight='./pretrained_weights/cifa10_cnn.hdf5')

class CatsVsDogsPredictResource:
    def on_post(self, request, response):
        form_data = cgi.FieldStorage(fp=request.stream, environ=request.env)
        image_file = form_data['image']

        try:
            filename = os.path.basename(image_file.filename)
            open('./input/images/' + filename, 'wb').write(image_file.file.read())

            result = cats_vs_dogs_classifier.predict(image_directory='./input/')
            result['predict'] = '{0:.2f}'.format(result['predict'])

            os.remove('./input/images/{}'.format(filename))

            response.status = falcon.HTTP_200
            response.body = json.dumps(result)
        except Exception as e:
            print(e)

            response.status = falcon.HTTP_505
            response.body = 'Something went wrong'

class Cifa10PredictPredictResource:
    def on_post(self, request, response):
        form_data = cgi.FieldStorage(fp=request.stream, environ=request.env)
        image_file = form_data['image']

        try:
            filename = os.path.basename(image_file.filename)
            open('./input/images/' + filename, 'wb').write(image_file.file.read())

            result = cifa10_classifier.predict(image_directory='./input/')
            result['predict'] = '{0:.2f}'.format(result['predict'])

            os.remove('./input/images/{}'.format(filename))

            response.status = falcon.HTTP_200
            response.body = json.dumps(result)
        except Exception as e:
            print(e)

            response.status = falcon.HTTP_505
            response.body = 'Something went wrong'

api = falcon.API(middleware=[HandleCORS()])

api.add_route('/cats-vs-dogs-predict', CatsVsDogsPredictResource())
api.add_route('/cifa10-predict', Cifa10PredictPredictResource())

