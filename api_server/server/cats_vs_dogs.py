import json

import numpy
import tensorflow.keras as keras
import tensorflow.keras.layers as keras_layers
import tensorflow.keras.models as keras_models
import tensorflow.keras.preprocessing as keras_preprocessing

class CatsVsDogsCNN:
    def predict(self, image_directory):
        test_data_generator = self.__test_image_gen.flow_from_directory(
            target_size=(64, 64),
            directory=image_directory,
            class_mode='categorical',
            batch_size=1,
            shuffle=False)

        predicts = self.__model.predict_generator(generator=test_data_generator)[0]

        class_index = numpy.where(predicts == max(predicts))[0][0]

        return {
            'predict': max(predicts) * 100,
            'class': self.labels[str(class_index)]}

    def __init__(self, cnn_config, pretrained_weight, labels):
        with open(labels) as file:
            self.labels = json.load(file)

        self.__test_image_gen = keras_preprocessing.image.ImageDataGenerator(rescale=1./255)
        self.__model = keras_models.load_model(pretrained_weight)