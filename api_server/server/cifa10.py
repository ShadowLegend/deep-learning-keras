import json

import numpy
import tensorflow.keras as keras
import tensorflow.keras.layers as keras_layers
import tensorflow.keras.models as keras_models
import tensorflow.keras.optimizers as keras_optimizers
import tensorflow.keras.losses as keras_losses
import tensorflow.keras.metrics as keras_metrics
import tensorflow.keras.preprocessing as keras_preprocessing

class Cifa10CNN:
    def predict(self, image_directory):
        test_data_generator = self.__test_image_gen.flow_from_directory(
            target_size=(32, 32),
            directory=image_directory,
            class_mode='categorical',
            batch_size=1,
            shuffle=False)

        predicts = self.__model.predict_generator(generator=test_data_generator)[0]

        class_index = numpy.where(predicts == max(predicts))[0][0]

        return {
            'predict': max(predicts) * 100,
            'class': self.labels[str(class_index)]}

    def __init__(self, cnn_config, pretrained_weight, labels):
        self.__test_image_gen = keras_preprocessing.image.ImageDataGenerator(rescale=1./255)

        with open(labels) as file:
            self.labels = json.load(file)

        with open(cnn_config) as file:
            config = json.load(file)

            image_channel_length = 3
            number_of_class = 2

            input_layer = keras_layers.Input(shape=(32, 32, image_channel_length))

            conv_layer_stack = input_layer

            convolutional_blocks = config['conv_block']

            for blocks in convolutional_blocks:
                conv_layer_stack = keras_layers.Conv2D(
                    filters=blocks['filter'],
                    kernel_size=blocks['kernel_size'],
                    padding='valid',
                    activation=keras.activations.relu)(conv_layer_stack)
                
                conv_layer_stack = keras_layers.MaxPooling2D(pool_size=blocks['pool_size'])(conv_layer_stack)

            layers = config['mlp']

            layer_stack = keras_layers.Flatten()(conv_layer_stack)

            for layer in layers:
                if 'neuron' in layer:
                    layer_stack = keras_layers.Dense(
                        units=layer['neuron'],
                        activation=keras.activations.relu,
                        use_bias=True)(layer_stack)

                if 'dropout' in layer:
                    layer_stack = keras_layers.Dropout(rate=layer['dropout'])(layer_stack)

            output_layer = keras_layers.Dense(
                units=10,
                activation=keras.activations.sigmoid,
                use_bias=True)(layer_stack)

            learning_rate = 0.001

            self.__model = keras_models.Model(inputs=input_layer, outputs=output_layer)

            self.__model.compile(
                optimizer=keras_optimizers.SGD(lr=learning_rate),
                loss=keras_losses.categorical_crossentropy,
                metrics=[keras_metrics.Recall(), keras_metrics.Precision()])

            self.__model.load_weights(pretrained_weight)
