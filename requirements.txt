numpy==1.16.3
opencv-contrib-python==4.1.0.25
Pillow==2.1.0
imutils==0.5.2
scikit-learn==0.21.1
matplotlib==3.0.3
pandas==0.24.2
h5py==2.9.0