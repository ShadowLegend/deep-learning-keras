import React from 'react';
import logo from './logo.svg';
import './App.css';

function readURL(file, setImage) {
  var reader = new FileReader();
    
  reader.onload = function(e) {
    setImage(e.target.result);
  }
  
  reader.readAsDataURL(file);
}

class App extends React.Component {
  state = {
    cvdSrc: '',
    cvdResult: '',
    c10Src: '',
    c10Result: ''
  }

  onChangeCVD = (event) => {
    const formData = new FormData()
    formData.append('image', event.target.files[0]);
    this.setState({ cvdResult: 'Predicting' });
    fetch('http://localhost:8000/cats-vs-dogs-predict', {method: 'post', body: formData}).then(async (response) => {
      const result = await response.json();
      this.setState({ cvdResult: `Result: ${result.predict}% ${result.class}` });
    })
    readURL(event.target.files[0], this.setCVDImage);
  }

  setCVDImage = (imageSrc) => {
    this.setState({
      cvdSrc: imageSrc,
    })
  }

  onChangeC10 = (event) => {
    const formData = new FormData()
    formData.append('image', event.target.files[0]);
    this.setState({ c10Result: 'Predicting' });
    fetch('http://localhost:8000/cifa10-predict', {method: 'post', body: formData}).then(async (response) => {
      const result = await response.json();
      this.setState({ c10Result: `Result: ${result.predict}% ${result.class}` });
    })
    readURL(event.target.files[0], this.setC10Image);
  }

  setC10Image = (imageSrc) => {
    this.setState({
      c10Src: imageSrc,
    })
  }

  render() {
    return (
      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
        <div style={{display: 'flex', flexDirection: 'column'}}>
          <h3>Cat vs Dog</h3>
          <img id="cvd-image" style={{width: 300, height: 300}} src={this.state.cvdSrc}/>
          <input type="file" id="cvd-image-input" onChange={this.onChangeCVD} />
          <h3 id="cvd-result">{this.state.cvdResult}</h3>
        </div>
        <div style={{width: 100}}></div>
        <div style={{display: 'flex', flexDirection: 'column'}}>
          <h3>Cifar 10</h3>
          <img id="cifa10-image" style={{width: 300, height: 300}} src={this.state.c10Src}/>
          <input type="file" id="cifa10-image-input" onChange={this.onChangeC10} />
          <h3 id="c10-result">{this.state.c10Result}</h3>
        </div>
      </div> 
    );
  }
};

export default App;
