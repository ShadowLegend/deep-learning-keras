## Deep Learning
Semester 6 University Project

## Table of Contents
- [About this project](#about-this-project)
- [How to run](#how-to-run)
  - [Notebook](#how-to-run-notebook)
  - [Server](#how-to-run-server)
  - [Web](#how-to-run-the-web)
- [Authors](#authors)

## About this project

This project is about studying about deep learning and using Keras with Tensorfor as backend as deep learning tool.

This repository contain notebooks(jupyter) to different ANN architectures and training on different dataset, simple web server to make use of trained weights and simple web interface for demo.

## How to run
### How to run notebook

  - Make sure `python3`, `opencv` is installed
  - Install `keras` and `tensorflow`, if you have [supported](https://developer.nvidia.com/cuda-gpus) nvidia GPU, follow this [link](https://medium.com/@abhijitdaskgp/setup-tensor-flow-and-keras-with-gpu-support-on-windows-pc-2a13f5f15f9f) for instruction to install `cuda` and `tensorflow` with GPU enabled
  - Install dependencies required for notebook by running `pip install -r requirements.txt`, make sure you are in this project root to do so
  - Install `jupyter` by running `pip install jupyter`
  - Start jupyter server by running `jupyter notebook`, make sure you are somewhere inside the project directory for easier to find those notebooks
  - Navigate to `notebook/modelling/` inside jupyter ui console and open corresponding notebook to see the codes and our code run checkpoint
  - The repo does not commit any dataset to version control simply because they are large, but each notebook will have link to download the dataset before you run the code again

### How to run server

  - Make sure `python3`, `make`, `opencv` is installed
  - Install dependencies required by running `pip install -r requirements.txt`, make sure you are in the `api_server` directory or point to correct path to the `requirements.txt`
  - Start the development server by running `make start.dev`
  - There are only two endpoint, `/cats-vs-dogs-predict` and `/cifa10-predict`, their request method are `POST` and required you to send one image file through `FormData` to it, the response will return as json like so `{ class: 'X', predict: 0.89 }`

### How to run the web

  - Make sure `node` is installed
  - Install dependencies required by running `npm install` and make sure you are in `web/cnn-demo` directory
  - Run `npm start` to start the development server
  - The server is required to start to be able to run the demo

## Authors

  - Piseth Chhom
  - Heng Ly
